<?php

declare(strict_types=1);

namespace Yunik;




/**
 * Yunik Swiss Army knife for PHP development
 */
class Utils {

    const EUROPE_WEBSERVICE_VAT_URL = "http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl";
	/**
	 * Generate a random string
	 * @param  integer $length length of the generated string
	 * @return string          generated string
	 */
	static function randomString($length = 10) : string
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	static function getDiscountFromTable(array $discountTable, $quantity)
	{	
		// No discount to be applyed
		if($discountTable[0]['minimum'] > $quantity)
			return 0;

		$counter = count($discountTable);
		foreach ($discountTable as $index => $discount) {
			
			// Is equal or last
			if(($discount['minimum'] == $quantity || $counter === $index + 1))
				return $discount['price'];

			$next = $discountTable[$index + 1]['minimum'];
			$hasOtherNext = $index + 2 === $counter;

			if((!$hasOtherNext && ($discount['minimum'] <= $quantity && $next > $quantity))
				|| ($discount['minimum'] < $quantity && $next > $quantity)) {
				return $discount['price'];
			}
		}
	}

    /**
     * Validate a VAT number using Europe Webservice
     * @param  string $vatNumber   the vat number
     * @param  string $countryCode the country code, ie: pt, dk
     * @return bool              true if valid, false otherwise
     */
	static function validateVat($vatNumber, $countryCode = null)
	{
		$vatNumber = str_replace(' ', '', trim($vatNumber));
        try{
        	if(empty($countryCode)) {
        		$countryCode = substr($vatNumber, 0, 2);
            	$vatNumber = substr($vatNumber, 2);
        	}

            $client = new \SoapClient(self::EUROPE_WEBSERVICE_VAT_URL);
            if($client){
                $params = array('countryCode' => $countryCode, 'vatNumber' => $vatNumber);
                    $r = $client->checkVat($params);
                    if($r->valid == true){
                        return true;
                    } else {
                        return false;
                    }
            } else {
                throw new BaseException("Can't connect to ec.europa.eu");
            }
        } catch (BaseException $e) {
            throw $e;
        }
    
		return false;
	}

	/**
     * As and Extra Check, you can check if
     * the specific VatID belongs to the country
     * that you want to check against. This is
     * useful when you need to charge VAT to local
     * business (Same country only)
     * @param  string $country
     * @return boolean
     */
    static function checkCountry($vatNumber, $countryCode){
        try{
            if(self::validateVat()){
                if(substr($vatNumber, 0, 2)==$countryCode){
                    return true;
                }else{
                    return false;
                }
            }
        } catch (Exception $e) {
            throw $ex;
        }
    }
}