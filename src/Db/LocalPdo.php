<?php

declare(strict_types=1);

namespace Yunik\Db;

use Yunik\BaseException,
    Yunik\Logger\YunikLogger;




class LocalPdo extends \PDO {

    static $useSocket;

    static $dbHost;

    static $dbName;

    static $dbUser;

    static $dbPassword;

    public function __construct()
    {
        try {
            // Format the dsn in case a socket is used
            $dsn = 'mysql:';
            $dsn .= self::$useSocket ? 'unix_socket=' : 'host=';
            $dsn .= self::$dbHost . ';dbname=' . self::$dbName;
            parent::__construct($dsn, self::$dbUser, self::$dbPassword);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            // always disable emulated prepared statement when using the MySQL driver
            $this->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
        }
        catch(\PDOException $ex) {
            YunikLogger::error("Error connecting to MySQL: " . $ex->getMessage());
            throw $ex;
        }
    }

    /**
     * Configure the PDOO
     * @param  string  $dbHost     database hostname
     * @param  string  $dbName     database name
     * @param  string  $dbUser     database user
     * @param  string  $dbPassword database user password
     * @param  boolean $useSocket  if a socket is used to connect to mysql
     */
    public static function config($dbHost, $dbName, $dbUser, $dbPassword = null, $useSocket = false)
    {
        self::$dbHost = $dbHost;
        self::$dbName = $dbName;
        self::$dbUser = $dbUser;
        self::$dbPassword = $dbPassword;
        self::$useSocket = $useSocket;
    }
}