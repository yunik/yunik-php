<?php

declare(strict_types=1);

namespace Yunik\Wp;

use Yunik\BaseException,
	Yunik\Wp\WpConnection;



/**
 * Key/Value implementation compatible with WP databae
 */ 
class Options {

	/**
	 * The instance of WpConnection used by this component to connect to
	 * the WP database
	 * @var WpConnection
	 */
	static $connection;

	/**
	 * The select query used to get a option
	 */
	const QUERY_SELECT = 'SELECT option_id, option_name, option_value, autoload FROM wp_options WHERE option_name=:option_name';

	/**
	 * The insert query used to create a option
	 */
	const QUERY_INSERT = 'INSERT INTO wp_options (option_name, option_value, autoload) VALUES (:option_name, :option_value, :autoload)';

	/**
	 * The delete query used to remove a option
	 */
	const QUERY_DELETE = 'DELETE FROM wp_options WHERE option_name = :option_name';

	/**
	 * Configure the component using the WpConnection instance
	 * @param  WpConnection $connection connection instance
	 */
	static function configure(WpConnection $connection) : void
	{
		self::$connection = $connection;
	}

	/**
	 * Return a option value or $default/null if not exists
	 * @param  string $key     the key name
	 * @param  string $default default value to return
	 * @return mixed|null          the key value or the default param if not exists
	 */
	public static function get(string $key, $default = null)
	{
		$stmt = self::$connection->getPdo()->prepare(self::QUERY_SELECT);
		$stmt->bindParam(':option_name', $key);
		$stmt->execute();
		$row = $stmt->fetch();
		
		return $row !== null ? $row['option_value'] : $default;
	}

	/**
	 * Set the value 
	 * @param string $key   option key
	 * @param mixed $value the option value
	 * @return the new item id
	 */
	public static function set(string $key, $value) : int
	{
		$pdo = self::$connection->getPdo();
		$autoload = 'no';
		$stmt = $pdo->prepare(self::QUERY_INSERT);
		$stmt->bindParam(':option_name', $key);
		$stmt->bindParam(':option_value', $value);
		$stmt->bindParam(':autoload', $autoload);
		$res = $stmt->execute();

		if(!$res) {
			throw new BaseException("Error inserting a new option in WP, execute returned false.");
		}

		return intval($pdo->lastInsertId());
	}

	/**
	 * Delete a key/value entry
	 * @param  string $key the key name
	 */
	public static function delete(string $key) : void
	{
		$pdo = self::$connection->getPdo();
		$stmt = $pdo->prepare(self::QUERY_DELETE);
		$stmt->bindParam(':option_name', $key);
		$res = $stmt->execute();

		if(!$res) {
			throw new BaseException("Error removing a option in WP, execute returned false.");
		}
	}
}