<?php

declare(strict_types=1);

namespace Yunik\Wp;

use Yunik\Logger\YunikLogger,
    Yunik\Db\LocalPdo,
    Yunik\Cache\YunikCacheProvider,
    Yunik\Cache\AbstractCacheProvider,
    Yunik\Interfaces\WpServiceInterface;




class WpKernelModule {

    static $services = [];

    static $plugins = [
        [
            'name' => 'WordPress SEO by Yoast',
            'slug' => 'wordpress-seo',
            'is_callable' => 'wpseo_init'
        ],
        [
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7'
        ],
        [
            'name' => 'Importador do Wordpress',
            'slug' => 'wordpress-importer'
        ],
        [
            'name' => 'Really Simple CAPTCHA',
            'slug' => 'really-simple-captcha'
        ],
        [
            'name' => 'Post Types Order',
            'slug' => 'post-types-order'
        ]
    ];

    protected $webServer = 'nginx';



    /**
     * Default settings defined when the plugin is activated
     */
    const SETTINGS_DEFAULTS = [  
    ];

    public static function add(WpServiceInterface $service) : void
    {
        self::$services[get_class($service)] = $service;
    }

    public static function logger() : void
    {
        $args = func_get_args();
        call_user_func_array(array(YunikLogger::class, 'config'), $args);
    }

    public static function db() : void
    {
        $args = func_get_args();
        call_user_func_array(array(LocalPdo::class, 'config'), $args);
    }

    public static function cache(AbstractCacheProvider $provider) : void
    {
        YunikCacheProvider::config($provider);
    }

    /**
     * Register more plugins
     * @param  array  $plugins the plugins to be registered
     */
    public static function plugins(array $plugins) : void
    {
        self::$plugins = array_merge(self::$plugins, $plugins);
    }

    /**
     * Start the WP Kernel Module
     * 
     * 
     * @return [type] [description]
     */
    public static function start() : void
    {
        add_action('tgmpa_register', '\Yunik\Wp\WpKernelModule::invokeTgmpa');

        foreach (self::$services as $key => $service) {
            $service->registerHooks();
        }
    }

    public static function invokeTgmpa() : void
    {
        if(!class_exists('tgmpa')) {
            return;
        }
        $config = array(
            'id'           => 'yunik',
            'default_path' => '',
            'menu'         => 'tgmpa-install-plugins',
            'parent_slug'  => 'plugins.php',
            'capability'   => 'manage_options',
            'has_notices'  => true,
            'dismissable'  => true,
            'dismiss_msg'  => '',
            'is_automatic' => false,
            'message'      => ''
        );

        tgmpa(self::$plugins, $config);
    }

    /**
     * Invoked when a post is deleted
     * @param  int $postId the post ID
     */
    public function onDeletePost($postId)
    {
        $post = get_post($postId);

        // Post should never be null as it was trashed, not deleted. Even so..
        if($post === null) {
            YunikLogger::error('Post with ID ' . $postId . ' not found.');
            return;
        }

        // If we can't write then something is wrong
        if(!is_writable(PLUGIN_REDIRECT_FILE)) {
            YunikLogger::error("Failed to add a link to the redirect list. The file isn\'t writtable, located at: " . PLUGIN_REDIRECT_FILE);
            return;
        }

        if($post->post_type !== 'product') {
            return;
        }

        $url = get_permalink($postId);

        $rule = 'rewrite ' . $url . ' ' . $this->cache->get(self::SETTINGS_DEFAULTS . 'rewrite');
        $file = fopen(PLUGIN_REDIRECT_FILE, 'a');
        fwrite($file, $rule . "\n");
        fclose($file);
    }

    /**
     * Reload the web server configuration
     */
    public function reloadWebServer() : void
    {
        // Ignore user aborts, otherwise PHP script will stop executing at this point
        ignore_user_abort(true);

        switch ($this->webServer) {
            case 'nginx':
                $this->reloadNginx();
                break;

            default:
                throw new \Exception("Plugin only supports Nginx webserver. Detected: " . $this->webServer);
                break;
        }
    }

    /**
     * Reload the nginx configuration
     */
    public function reloadNginx() : void
    {
        exec(PLUGIN_ROOT . '/includes/reload-nginx.sh');
    }

    /**
     * Invoked when the plugin is activated
     */
    public function setupPlugin() : void
    {
        $keys = $this->setupDefaultSettings();
        YunikLogger::debug(count($keys) > 0 ? 'Default keys: ' . print_r($keys) : 'No default keys saved');

        if(!$this->isInstalled()) {
            $this->installPlugin();
        }
    }

    /**
     * Indicates if the plugin was already configured
     * @return boolean [description]
     */
    public function isInstalled() : bool
    {
        return $this->cache->get(self::PREFIX . 'installed') ?? false;
    }


    /**
     * Configure the plugin with the default settings
     */
    public function setupDefaultSettings() : array
    {
        // Keep track of which defaults were created
        $keys = [];
        foreach (self::SETTINGS_DEFAULTS as $key => $default) {
            // None of settings should be empty
            if(empty($this->cache->get(self::SETTINGS_PREFIX . $key))) {
                $this->cache->set(self::SETTINGS_PREFIX . $key, $default);
                $keys[] = $key;
            }
        }

        return $keys;
    }

    /**
     * Helper method to echo a list of result with a table
     * @param  array  $headers  the table headers, ie: array('Id', 'ProductId', 'Sku')
     * @param  array  $messages the table tbody, ie: array(array(1, 2, 'sku_1'), array(2, 3, 'sku_2'))
     * @return [type]           [description]
     */
    protected function echoResultTable(array $headers, array $messages)
    {
        $output = '<table class="wp-list-table widefat fixed striped"><thead><tr>';
        foreach ($headers as $header) {
            $output .= '<td>' . $header . '</td>';
        }

        $output .= '</tr></thead><tbody>';

        foreach ($messages as $message) {
            $output .= '<tr>';
            foreach ($message as $entry) {
                if(!is_string($entry)){
                    $entry = $this->parseValueAsString($entry);
                }
                $output .= "<td>$entry</td>";
            }
            $output .= '</tr>';
        }

        $output .= '</tr></tbody></table>';
        return $output;
    }

    /**
     * Helper method to format values into string
     * @param  mixed $value the value
     * @return string        a readable version of the value
     */
    private function parseValueAsString($value) : string
    {
        if(is_array($value)) {
            return implode(', ', $value);
        }

        return print_r($value, true);

    }

    /**
     * Calculate the time elapsed from a two periods
     *
     * Start end End parameters can be obtained with microtime(true)
     * and to return as float
     * @source https://stackoverflow.com/questions/7850259/calculate-elapsed-time-in-php
     * @param  float  $start the start
     * @param  float  $end   the end
     * @return string        the time in hh:mm:ss
     */
    protected function calculateTimeElapsed(float $start, float $end) : string
    {
        $timeDiff = $end - $start;
        $h = floor($timeDiff / 3600);
        $timeDiff -= $h * 3600;
        $m = floor($timeDiff / 60);
        $timeDiff -= $m * 60;
        return $h.':'.sprintf('%02d', $m).':'.sprintf('%02d', $timeDiff);
    }
}