<?php

declare(strict_types=1);

namespace Yunik\Wp\Interfaces;




interface RegisterScriptsInterface {

	public function registerScripts() : void;
}