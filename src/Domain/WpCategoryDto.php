<?php

namespace Yunik\Domain;




class WpCategoryDto {

	protected $id;

	protected $name;

	protected $parent;

	public function __construct($categoryId, $name = null, $parent = null)
	{
		$this->id = $categoryId;
		$this->name = $name;
		$this->parent = $parent;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setId($id) : void
	{
		$this->id = $id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name) : void
	{
		$this->name = $name;
	}

	public function getParent()
	{
		return $this->parent;
	}

	public function setParent($parent) : void
	{
		$this->parent = $parent;
	}
}