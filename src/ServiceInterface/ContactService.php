<?php

declare(strict_types=1);

namespace Yunik;

use Yunik\BaseException,
	Yunik\ValidatorException,
	Yunik\ValidatorException,
	Yunik\Wp\WpServiceConfig,
	Yunik\Wp\WpServiceAbstract;

class ContactService extends WpServiceAbstract {

	static $postContactFields = ['recipient', 'subject', 'body'];
	public function __construct()
	{
		parent::__construct(new WpServiceConfig(
			__('Doctor', 'cssm'),
			__('Doctors', 'cssm'), 
			'doctor', 
			'doctor',
			'doctor',
			'cssm/v1'
		));
	}

	public function registerScripts() : void
	{
	}

    public function registerAdminScripts() : void
    {
    }

    public function registerApiHooks() : void
    {
    	register_rest_route($this->config->getRestNamespace(), '/contact', array(
			'methods' => \WP_REST_Server::CREATABLE,
			'callback' => array($this , 'postContact')
	    ));
    }

    public function postContact(\WP_REST_Request $request)
	{
		try {
			$this->assertRequestFields($request, self::$postContactFields, true);
			return rest_ensure_response(array('ok' => true));
		}
		catch(ValidatorException $ex) {
			return ValidatorException::response($ex);
		}
		catch(BaseException $ex) {
			return $this->unexpectedApiError($ex);
		}
		
	}
}