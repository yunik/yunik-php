<?php

declare(strict_types=1);

namespace Yunik\Interfaces;




interface WpServiceOnPostSave {

	public function onSave(int $postId, \Wp_Post $post, bool $update);
}