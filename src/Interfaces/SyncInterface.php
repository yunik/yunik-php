<?php

namespace Yunik\Interfaces;

use Yunik\Domain\WpProductDto,
	Yunik\Domain\WpCategoryDto,
	Yunik\Domain\ListOperationResult;




interface SyncInterface {

	public function productExists(string $rcsoft) : bool;
	
	// Product
	public function productIsPending($externalId) : bool;

	public function productIsFailed($externalId) : bool;

	public function productIsEqual(WpProductDto $product, $list = null) : bool;

	public function getPendingProducts($skip = 0, $take = 20) : array;

	public function getFailedProducts($skip = 0, $take = 20) : array;

	public function removeProductFromPending(string $externalId) : ListOperationResult;

	public function removeProductFromFailed(string $externalId) : ListOperationResult;

	public function addProductToPending(WpProductDto $product) : ListOperationResult;

	public function addProductToFailed(WpProductDto $product) : ListOperationResult;

	public function markProductAsCreated($externalId) : ListOperationResult;

	public function markProductAsFailed($externalId) : ListOperationResult;

	// Category
	public function clearCategoryLists() : void;

	public function countCategories($list) : int;

	public function categoryIsPending($externalId) : bool;

	public function categoryIsFailed($externalId) : bool;

	public function getPendingCategories($skip = 0, $take = 20) : array;

	public function getFailedCategories($skip = 0, $take = 20) : array;

	public function removeCategoryFromPending(string $externalId) : ListOperationResult;

	public function removeCategoryFromFailed(string $externalId) : ListOperationResult;

	public function addCategoryToPending(WpCategoryDto $category) : ListOperationResult;

	public function addCategoryToFailed($categoryId) : ListOperationResult;

	public function markCategoryAsCreated($categoryId, $externalId);

	public function markCategoryAsFailed($externalId);

	// Product Map
	public function productMapExists(int $woocommerce, string $rcsoft) : bool;

	public function productMapExistsByRcsoft(string $rcsoft) : bool;

	public function addProductMap(?string $woocommerce, string $rcsoft);

	public function removeProductMap(int $woocommerce, int $rcsoft) : bool;

	public function getProductMap(int $mapId);

	public function getProductMapByWoocommerce(int $woocommerce) : array;

	public function getProductMapByRcsoft(string $rcsoft) : array;

	public function clearProductLists() : void;

	public function countProducts(string $list) : int;

	public function getProductFromQueue(int $queueId) : array;

}