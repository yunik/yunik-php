<?php

namespace Tests;

use Tests\PHPUnitUtil,
	PHPUnit\Framework\TestCase,
	Yunik\Utils,
	Yunik\AbstractApp,
	Yunik\Cache\WpCacheProvider;




class MockApp extends AbstractApp
{

	public function __construct()
	{
        $this->cache = new WpCacheProvider();
	}

	public function importNewProducts() : bool
	{
		return false;
	}

	public function createProduct($item)
	{

	}

	public function parseItems(array &$items) : void
	{

	}
}

class AbstractAppTest extends TestCase
{

	protected $app;

	protected $hookName;

	public function setUp()
	{
		$this->app = new MockApp();
		$this->hookName = Utils::randomString(30);
	}

	public function testCanCleanProducts()
	{
		$this->app->cronCleanExecute();
	}

	public function testCanUnlockCronAfterExpired() : void
	{
		// Need to lock the cron to assert the expiration status
		PHPUnitUtil::callMethod($this->app, 'lockCron');
		$this->assertTrue($this->app->cronIsLocked());

		// Cant be expired
		$isExpired = PHPUnitUtil::callMethod($this->app, 'cronIsExpired');
		$this->assertFalse($isExpired);
		sleep(2);

		// Slept for 2 minutes, must be expired
		$isExpired = PHPUnitUtil::callMethod($this->app, 'cronIsExpired', [1]);
		$this->assertTrue($isExpired);

		// Cant be expired, 2 minutes
		$isExpired = PHPUnitUtil::callMethod($this->app, 'cronIsExpired', [120]);
		$this->assertFalse($isExpired);
	}
}