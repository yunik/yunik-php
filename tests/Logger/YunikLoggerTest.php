<?php

namespace Tests\Logger;

use PHPUnit\Framework\TestCase,
	Yunik\Logger\Logger,
	Yunik\Logger\YunikLogger;




class YunikLoggerTest extends TestCase {

	public function setUp() : void
	{
		
	}

	/**
	 * Assert that the logger can write to the console
	 * using ob_ methods to capture the output
	 */
	public function testCanInjectLogger() : void
	{
		YunikLogger::config(new Logger());
		YunikLogger::debug('testing');
	}
}